The issue
=========

We [need](https://www.ipcc.ch) to save energy. One (small) way could be to
shutdown servers at night, if nobody uses them. Note that increasing the number
of boots may decrease lifetime of electronical components, especially hard
drives. Choosing if it is better to save electricity or extend components
lifetime is unfortunately not that simple…

Example: a Dell T110 II with 3 3,5" hard drives consumes between 32 and 55W. If
shutdown between midnight and 7 AM, it saves between 81 and 140 kWh a year.

While automatic shutdown is easy, the issue is that many BIOS/UEFI do not allow
scheduling automatic boot, like the Dell T100 II in our example. Fortunately,
your computer or server motherboard probably supports *RTC (real-time clock)
wakeups*, which can be scheduleed from Linux. For further reading on this
subject, you can read the great
[MythTV documentation](https://www.mythtv.org/wiki/ACPI_Wakeup).

The solution
============

The command `rtcwake` from `util-linux` package can schedule a RTC wakeup,
and optionaly put the system in sleep or shutdown mode.

To accomodate with every cases (power loss, manual shutdown, manual wakeup),
make sure you to run it:

- at boot **and**
- just after the scheduled boot time

Also, make sure your machine is configured to boot automatically when power
returns (in case of power loss).

The MythTV documentation also
[recommends](https://www.mythtv.org/wiki/ACPI_Wakeup#Disable_hwclock_updates)
to disable automatic writing to RTC at shutdown time, but this requirement
seems hardware-specific, and at least not needed for the Dell T110 II.

Previous solution
=================

Before knowing about `rtcwake`, I wrote the script called
`schedule-tomorrow-boot`, which is intended to schedule automatic boot of a
computer/server *every day* of the week. Usage is simple:

```sh
# schedule-tomorrow-boot HOUR MINUTE
```

Where HOUR (0-23) and MINUTE (0-59) are the wakeup time for next day.

This script works only if your RTC is in *UTC time*, which is the default if
there is no dual boot with Windows.

The wakeup time will be wrong on days the server changes its time (if it is in
a country that uses daylight saving times).
